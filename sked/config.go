package main

import (
	"encoding/json"
	"os"
	"path/filepath"
)

// Config object from unmarshalled configuration file.
type Config struct {
	Nodes []string `json:"nodes"`
	// Tables   []map[string]map[string]string `json:"tables"`
	Keyspace string `json:"keyspace"`
}

// LoadConfig file into the Config struct
func LoadConfig(fName string) (Config, error) {
	var gConf Config
	fAbsPath, err := filepath.Abs(fName)
	if err != nil {
		return gConf, err
	}
	fConf, err := os.Open(fAbsPath)
	if err != nil {
		return gConf, err
	}
	defer fConf.Close()

	jsonParser := json.NewDecoder(fConf)
	jsonParser.Decode(&gConf)
	return gConf, nil
}
