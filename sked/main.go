package main

import (
	"fmt"
	"log"
	"path/filepath"
	"strings"
	"time"

	"github.com/gocql/gocql"
)

func main() {
	// Open conf and unmarshal into a Configuration struct.
	gConf, err := LoadConfig("../config/conf.json")
	if err != nil {
		log.Fatalf("No config file could be found: \n%v", err)
	}
	fmt.Printf("gConf = %+v\n", gConf)

	cluster := gocql.NewCluster(gConf.Nodes...) // connect to the cluster
	cluster.Timeout = time.Duration(10) * time.Second
	cluster.Keyspace = gConf.Keyspace // use this as the default kespace
	cluster.Consistency = gocql.Quorum
	session, err := cluster.CreateSession()
	if err != nil {
		log.Fatalf("Something went wrong trying to connect to the DB: \n%v", err)
	}
	defer session.Close()

	tableInfo, err := LoadSchema("../config/schema.json")
	if err != nil {
		log.Fatalf("The following error occured when parsing table schema: \n%v", err)
	}

	// Check if the SQL MAIN tables exist. Create them in they don't
	keySpaceMeta, _ := session.KeyspaceMetadata(gConf.Keyspace)
	for k, v := range tableInfo {
		// Cassandra uses lowercase for names
		tableName := strings.ToLower(k)
		if _, exists := keySpaceMeta.Tables[tableName]; exists != true { // generate create table query
			fmt.Printf("\n\n%s does not exist in %s.\n", k, gConf.Keyspace)
			tableCols := v.Columns
			query := fmt.Sprintf("CREATE TABLE %s (", k)
			for colName, colType := range tableCols {
				query += fmt.Sprintf(" %s %s,", colName, colType)
			}
			primaryKey := fmt.Sprintf(" PRIMARY KEY (%s))", v.Primary)
			query += primaryKey
			fmt.Printf("Executing %s\n", query)
			err = session.Query(query).Exec()
			if err != nil {
				log.Printf("There was a problem creating %s. Continuing.", k)
			}
		}
	}

	fmt.Println()
	fAbsPath, _ := filepath.Abs("../assets/")
	WriteTables(session, tableInfo, fAbsPath)

	fmt.Printf("\n\nDone.")
}
