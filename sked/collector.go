package main

import (
	"encoding/csv"
	"fmt"
	"io"

	"github.com/gocql/gocql"
)

// WorkQueue is a buffered channel to put new work into
var WorkQueue = make(chan WorkRequest, 10000)

// Collector adds work requests to put into the WorkRequest channel
func Collector(r *csv.Reader, table string, rowColOrder []string,
	dateColLocs []int, colInfo *Table, session *gocql.Session, nWorkers int) {
	i := 0

	d := NewDispatcher(nWorkers)
	d.Run()

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			fmt.Println(err)
			return
		}
		work := WorkRequest{Row: &record, Table: &table, RowOrder: &rowColOrder, DateLocs: &dateColLocs, ColInfo: colInfo, Session: session, ID: i + 1}
		WorkQueue <- work
		i++
		// fmt.Printf("len(WorkQueue) = %+v\n", len(WorkQueue))
		if len(WorkQueue) == 9999 {
			fmt.Println("\nWork queue full. Waiting for the workers to catch up.")
			for {
				// fmt.Printf("len(WorkQueue) = %+v\n", len(WorkQueue))
				if len(WorkQueue) <= 1 {
					fmt.Println("Caught up.")
					break
				}
			}
		}
		// fmt.Printf("\rQueued %d lines.", i)
	}
}
