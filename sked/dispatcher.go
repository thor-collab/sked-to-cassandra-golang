package main

// Dispatcher contains the pools of workers.
type Dispatcher struct {
	WorkerQueue chan chan WorkRequest
	nWorkers    int
	QuitChan    chan bool
	WorkerList  []Worker
}

//NewDispatcher initializes a poll to assign work to
func NewDispatcher(nWorkers int) *Dispatcher {
	pool := make(chan chan WorkRequest, nWorkers)
	return &Dispatcher{WorkerQueue: pool, nWorkers: nWorkers}
}

// Run starts the dispatcher for WorkerQueues
func (d *Dispatcher) Run() {

	// Now, create all of our workers.
	for i := 0; i < d.nWorkers; i++ {
		// fmt.Println("Starting worker", i+1)
		worker := NewWorker(i+1, d.WorkerQueue)
		d.WorkerList = append(d.WorkerList, worker)
		worker.Start()
	}

	go func() {
		for {
			select {
			case <-d.QuitChan:
				return
			case work := <-WorkQueue:
				go func(work WorkRequest) {
					worker := <-d.WorkerQueue

					worker <- work
				}(work)
			}
		}
	}()
}

// Stop stops the workers once the work to be done is completed.
func (d *Dispatcher) Stop() {
	for _, worker := range d.WorkerList {
		worker.Stop()
	}
	d.WorkerList = nil
	d.QuitChan <- true
}
