package main

import "github.com/gocql/gocql"

// WorkRequest objects have the object of work to operate on and a UUID for the job
type WorkRequest struct {
	Row      *[]string
	Table    *string
	RowOrder *[]string
	DateLocs *[]int
	ColInfo  *Table
	Session  *gocql.Session
	ID       int
}
