package main

import (
	"encoding/csv"

	"github.com/gocql/gocql"
)

// ForkWork takes a number to create workers and a file to iterate through
func ForkWork(nWorkers int, csvReader *csv.Reader, tableName string,
	rowOrder []string, dateColLocs []int, colInfo *Table, session *gocql.Session) {

	Collector(csvReader, tableName, rowOrder, dateColLocs, colInfo, session, nWorkers)
}
