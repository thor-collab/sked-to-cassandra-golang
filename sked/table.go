package main

import (
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/gocql/gocql"
)

// Table holds basic information about a table in SQL to be mirrored into Cassandra.
type Table struct {
	Columns map[string]string
	Primary string
}

// LoadSchema takes a json file and parses it to return Table information. It returns
// map[string]*Table. Tables can be referenced by name.
func LoadSchema(fName string) (map[string]*Table, error) {
	var xSchema interface{}
	tableInfo := make(map[string]*Table)
	fAbsPath, err := filepath.Abs(fName)
	if err != nil {
		return tableInfo, err
	}
	fSchema, err := ioutil.ReadFile(fAbsPath)
	if err != nil {
		return tableInfo, err
	}

	json.Unmarshal(fSchema, &xSchema)
	mSchema := xSchema.(map[string]interface{})

	for key, val := range mSchema {
		switch tableCols := val.(type) {
		case map[string]interface{}:
			fmt.Printf("\n\nFound table schema file %s.", key)
			p := new(Table)
			p.Columns = make(map[string]string)
			p.Primary = ""
			for key2, val2 := range tableCols {
				switch colVal := val2.(type) {
				case string:
					colProperties := strings.Split(colVal, ",")
					p.Columns[key2] = strings.TrimSpace(colProperties[0]) // first property is always type
					if len(colProperties) > 1 {
						for _, v := range colProperties[1:] {
							cleanedProp := strings.TrimSpace(v)
							if cleanedProp == "primary" { // second property is always key type
								p.Primary += strings.TrimSpace(key2) + ","
							}
						}
					}
				default:
					fmt.Println(key2, " is of a type I don't know how to handle.")
					return tableInfo, errors.New("the schema parameter was improperly formatted, review schema.json and rerun")
				}
			}
			p.Primary = strings.TrimRight(p.Primary, ",")
			if p.Primary == "" {
				uuidCol := strings.ToLower(key[:len(key)-1]) + "_uuid"
				p.Columns[uuidCol] = "uuid"
				p.Primary = uuidCol
			}
			tableInfo[key] = p
		default:
			fmt.Println(key, " is of a type I don't know how to handle.")
			return tableInfo, errors.New("the schema parameter was improperly formatted, review schema.json and rerun")
		}
	}
	return tableInfo, nil
}

// WriteTables takes a directory and Table map and parses csv or tsv data files, validating table data. The function
// then writes the files files to cassandra.
func WriteTables(session *gocql.Session, tableInfo map[string]*Table, dName string) error {
	files, err := ioutil.ReadDir(dName)
	if err != nil {
		return err
	}

	// numCPUs := runtime.NumCPU()
	// runtime.GOMAXPROCS(numCPUs + 1) // numCPUs hot threads + one for async tasks.

	// pool, _ := tunny.CreatePoolGeneric(500).Open()
	// fmt.Printf("numCPUs = %+v\n", numCPUs)

	// defer pool.Close()

	for _, file := range files {
		fmt.Printf("\n\nReading %s.\n", file.Name())
		parseInfo := strings.Split(file.Name(), ".")

		colInfo, present := tableInfo[parseInfo[0]]
		dateColNames := ""
		if present != true {
			fmt.Println("\n\nThe table data file did not match any table in Cassandra.")
			continue
		} else {
			for colName, colType := range colInfo.Columns {
				// Check for columns that are dates, store their names
				if strings.Contains("datetimestamp", colType) {
					dateColNames += fmt.Sprintf("%s,", colName)
				}
			}
			dateColNames = strings.TrimRight(dateColNames, ",")
		}

		fTable, err := os.Open(dName + "/" + file.Name())
		if err != nil {
			log.Printf("No data file could be found: \n%v\n\nContinuing to next data file.\n", err)
		}

		var dateColLocs []int
		csvReader := csv.NewReader(fTable)
		if parseInfo[1] == "csv" {
			csvReader.Comma = ','
		} else {
			csvReader.Comma = '\t'
		}

		// read the first line line to get overall column order and locations of dates
		record, _ := csvReader.Read()
		// records, _ := csvReader.ReadAll()
		var rowColOrder []string

		for i, v := range record {
			rowColOrder = append(rowColOrder, strings.ToLower(v))
			if strings.Contains(dateColNames, strings.ToLower(v)) {
				dateColLocs = append(dateColLocs, i)
			}
		}
		// i := 0
		// for i, record := range records[1:] {
		//     // record, err := csvReader.Read()
		//     if err == io.EOF {
		//         break
		//     } else if err != nil {
		//         fmt.Println(err)
		//         break
		//     }
		//     i++
		//     fmt.Printf("\rRead %d lines.", i)
		//
		//     _, err = pool.SendWork(func() { GenQuery(record, rowColOrder, parseInfo[0], dateColLocs, colInfo, session) })
		// }

		//     work := WorkRequest{Row: record, Table: table, RowOrder: rowColOrder, DateLocs: dateColLocs, ColInfo: colInfo, Session: session, ID: i + 1}
		//     WorkQueue <- work
		//     i++
		//     fmt.Printf("\rQueued %d lines.", i)
		//     if i%1000000 == 0 {
		//         fmt.Println("\nStopping to let workers process.")
		//         time.Sleep(10 * time.Second)
		//     }
		// }

		ForkWork(500, csvReader, parseInfo[0], rowColOrder, dateColLocs, colInfo, session)

		fTable.Close()
	}

	return nil
}

// GenQuery creates a string to use to send to Cassandra
/*
 * func GenQuery(record []string, rowColOrder []string, tableName string, dateColLocs []int, colInfo *Table, session *gocql.Session) {
 *     var err error
 *     var buffer bytes.Buffer
 *     lowerName := strings.ToLower(tableName)
 *     lowerType := lowerName[0 : len(lowerName)-1]
 *     buffer.WriteString("INSERT INTO ")
 *     buffer.WriteString(lowerName)
 *
 *     var queryColOrder []string
 *     queryValOrder := ""
 *
 *     // iterate through the row and append to the query
 *     dateLocSet := make(map[int]bool) // create a set for O(1) lookup of matching date columns
 *     for _, v := range dateColLocs {
 *         dateLocSet[v] = true
 *     }
 *
 *     for i, v := range record {
 *         var parsedDate time.Time
 *         layout := "02-Jan-06"
 *         if v == "" { // if the column has no value, don't bother adding to INSERT
 *             continue
 *         }
 *         // append to the current Column order, the column for the query
 *         queryColOrder = append(queryColOrder, rowColOrder[i])
 *         _, dateCol := dateLocSet[i]
 *         if dateCol {
 *             parsedDate, err = time.Parse(layout, v)
 *             if err != nil { // if the date can't be parsed, just don't insert
 *                 log.Println("Error parsing date:", err)
 *                 continue
 *             }
 *             buffer.WriteString("$$")
 *             buffer.WriteString(parsedDate.Format("2006-01-02"))
 *             buffer.WriteString("$$,")
 *             queryValOrder += fmt.Sprintf("$$%s$$,", parsedDate.Format("2006-01-02"))
 *         } else { // if the column is not a date or timestamp, add to query
 *             if colInfo.Columns[rowColOrder[i]] == "text" {
 *                 queryValOrder += fmt.Sprintf("$$%s$$,", v)
 *                 buffer.WriteString("$$")
 *                 buffer.WriteString(v)
 *                 buffer.WriteString("$$,")
 *             } else if colInfo.Columns[rowColOrder[i]] == "uuid" {
 *                 uuid, _ := gocql.RandomUUID()
 *                 buffer.WriteString("$$")
 *                 buffer.WriteString(uuid.String())
 *                 buffer.WriteString("$$,")
 *             } else if colInfo.Columns[rowColOrder[i]] == "boolean" {
 *                 queryValOrder += fmt.Sprintf("%t,", v != "0")
 *                 buffer.WriteString(uuid.String())
 *             } else {
 *                 _, err = strconv.ParseFloat(v, 64)
 *                 if err != nil {
 *                     queryValOrder += fmt.Sprintf("$$%s$$,", v)
 *                 } else {
 *                     queryValOrder += fmt.Sprintf("%s,", v)
 *                 }
 *             }
 *         }
 *     }
 *     _, ok := colInfo.Columns[strings.ToLower(tableName[0:len(tableName)-1]+"_uuid")]
 *     if ok {
 *         queryColOrder = append(queryColOrder, strings.ToLower(tableName[0:len(tableName)-1]+"_uuid"))
 *         uuid, _ := gocql.RandomUUID()
 *         queryValOrder += fmt.Sprintf("%v,", uuid)
 *     }
 *
 *     queryValOrder = strings.TrimRight(queryValOrder, ",")
 *     query := fmt.Sprintf("%s (%s) VALUES (%s)", queryInsert, strings.Join(queryColOrder, ","), queryValOrder)
 *     err = session.Query(query).Exec()
 *     if err != nil {
 *         fmt.Printf("err = %+v\n", err)
 *     }
 * }
 */
