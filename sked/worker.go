package main

import (
	"bytes"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/gocql/gocql"
)

// Worker contains a channel to listen for jobs and a channel of channels to register as a worker
type Worker struct {
	ID          int
	Work        chan WorkRequest
	WorkerQueue chan chan WorkRequest
	QuitChan    chan bool
}

// NewWorker creates, and returns a new Worker object. Its only argument
// is a channel that the worker can add itself to whenever it is done its
// work.
func NewWorker(id int, workerQueue chan chan WorkRequest) Worker {
	// Create, and return the worker.
	worker := Worker{
		ID:          id,
		Work:        make(chan WorkRequest),
		WorkerQueue: workerQueue,
		QuitChan:    make(chan bool),
	}

	return worker
}

// Start function "starts" the worker by starting a goroutine, that is
// an infinite "for-select" loop.
func (w *Worker) Start() {
	go func() {
		for {
			// Add ourselves into the worker queue.,
			w.WorkerQueue <- w.Work
			select {
			case work := <-w.Work:
				{
					var err error
					var valBuffer bytes.Buffer
					var buffer bytes.Buffer
					// Receive a work request.
					record := work.Row
					tableName := work.Table
					rowColOrder := *work.RowOrder
					dateColLocs := work.DateLocs
					colInfo := work.ColInfo
					session := work.Session
					// id := work.ID
					lowerName := strings.ToLower(*tableName)
					lowerUUID := lowerName[0:len(lowerName)-1] + "_uuid"
					buffer.WriteString("INSERT INTO ")
					buffer.WriteString(lowerName)
					buffer.WriteString(" (")

					var queryColOrder []string

					// iterate through the row and append to the query
					dateLocSet := make(map[int]bool) // create a set for O(1) lookup of matching date columns
					for _, v := range *dateColLocs {
						dateLocSet[v] = true
					}

					for i, v := range *record {
						var parsedDate time.Time
						layout := "02-Jan-06"
						if v == "" { // if the column has no value, don't bother adding to INSERT
							continue
						}
						// append to the current Column order, the column for the query
						queryColOrder = append(queryColOrder, rowColOrder[i])
						_, dateCol := dateLocSet[i]
						if dateCol {
							parsedDate, err = time.Parse(layout, v)
							if err != nil { // if the date can't be parsed, just don't insert
								log.Println("Error parsing date:", err)
								continue
							}
							valBuffer.WriteString("$$")
							valBuffer.WriteString(parsedDate.Format("2006-01-02"))
							valBuffer.WriteString("$$,")
						} else { // if the column is not a date or timestamp, add to query
							if colInfo.Columns[rowColOrder[i]] == "text" {
								valBuffer.WriteString("$$")
								valBuffer.WriteString(v)
								valBuffer.WriteString("$$,")
							} else if colInfo.Columns[rowColOrder[i]] == "uuid" {
								uuid, _ := gocql.RandomUUID()
								valBuffer.WriteString("$$")
								valBuffer.WriteString(uuid.String())
								valBuffer.WriteString("$$,")
							} else if colInfo.Columns[rowColOrder[i]] == "boolean" {
								if v != "0" {
									valBuffer.WriteString("true,")
								} else {
									valBuffer.WriteString("false,")
								}
							} else {
								_, err = strconv.ParseFloat(v, 64)
								if err != nil {
									valBuffer.WriteString("$$")
									valBuffer.WriteString(v)
									valBuffer.WriteString("$$,")
								} else {
									valBuffer.WriteString(v)
									valBuffer.WriteString(",")
								}
							}
						}
					}
					_, ok := colInfo.Columns[lowerUUID]
					if ok {
						queryColOrder = append(queryColOrder, lowerUUID)
						uuid, _ := gocql.RandomUUID()
						valBuffer.WriteString(uuid.String())
						valBuffer.WriteString(",")
					}
					colVals := strings.Join(queryColOrder, ",")

					valBuffer.Truncate(valBuffer.Len() - 1)
					buffer.WriteString(colVals)
					buffer.WriteString(") VALUES (")
					buffer.WriteString(valBuffer.String())
					buffer.WriteString(")")
					// fmt.Printf("buffer.String() = %+v\n", buffer.String())
					// query := fmt.Sprintf("%s (%s) VALUES (%s)", queryInsert, strings.Join(queryColOrder, ","), queryValOrder)
					err = session.Query(buffer.String()).Exec()
					if err != nil {
						fmt.Printf("buffer.String() = %+v\n", buffer.String())
						fmt.Printf("err = %+v\n", err)
					}
					// fmt.Printf("\rCompleted line %d.", id)
				}
			case <-w.QuitChan:
				// We have been asked to stop.,
				// fmt.Printf("worker%d stopping\n", w.ID)
				return
			}
		}
	}()
}

// Stop tells the worker to stop listening for work requests.
//
// Note that the worker will only stop *after* it has finished its work.
func (w *Worker) Stop() {
	go func() {
		w.QuitChan <- true
	}()
}
