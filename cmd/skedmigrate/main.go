package main

import (
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/gocql/gocql"
)

// Config object from unmarshalled configuration file.
type Config struct {
	Nodes []string `json:"nodes"`
	// Tables   []map[string]map[string]string `json:"tables"`
	Keyspace string `json:"keyspace"`
}

// Table holds basic information about a table in SQL to be mirrored into Cassandra.
type Table struct {
	Columns map[string]string
	Primary string
}

// LoadConfig file into the Config struct
func LoadConfig(fName string) (Config, error) {
	var gConf Config
	fAbsPath, err := filepath.Abs(fName)
	if err != nil {
		return gConf, err
	}
	fConf, err := os.Open(fAbsPath)
	if err != nil {
		return gConf, err
	}
	defer fConf.Close()

	jsonParser := json.NewDecoder(fConf)
	jsonParser.Decode(&gConf)
	return gConf, nil
}

// LoadSchema takes a json file and parses it to return Table information. It returns
// map[string]*Table. Tables can be referenced by name.
func LoadSchema(fName string) (map[string]*Table, error) {
	var xSchema interface{}
	tableInfo := make(map[string]*Table)
	fAbsPath, err := filepath.Abs("../../config/schema.json")
	if err != nil {
		return tableInfo, err
	}
	fSchema, err := ioutil.ReadFile(fAbsPath)
	if err != nil {
		return tableInfo, err
	}

	json.Unmarshal(fSchema, &xSchema)
	mSchema := xSchema.(map[string]interface{})

	for key, val := range mSchema {
		switch tableCols := val.(type) {
		case map[string]interface{}:
			fmt.Printf("\n\nFound table schema file %s.", key)
			p := new(Table)
			p.Columns = make(map[string]string)
			p.Primary = ""
			for key2, val2 := range tableCols {
				switch colVal := val2.(type) {
				case string:
					colProperties := strings.Split(colVal, ",")
					p.Columns[key2] = strings.TrimSpace(colProperties[0]) // first property is always type
					if len(colProperties) > 1 {
						for _, v := range colProperties[1:] {
							cleanedProp := strings.TrimSpace(v)
							if cleanedProp == "primary" { // second property is always key type
								p.Primary += strings.TrimSpace(key2) + ","
							}
						}
					}
				default:
					fmt.Println(key2, " is of a type I don't know how to handle.")
					return tableInfo, errors.New("the schema parameter was improperly formatted, review schema.json and rerun")
				}
			}
			p.Primary = strings.TrimRight(p.Primary, ",")
			if p.Primary == "" {
				uuidCol := strings.ToLower(key[:len(key)-1]) + "_uuid"
				p.Columns[uuidCol] = "uuid"
				p.Primary = uuidCol
			}
			tableInfo[key] = p
		default:
			fmt.Println(key, " is of a type I don't know how to handle.")
			return tableInfo, errors.New("the schema parameter was improperly formatted, review schema.json and rerun")
		}
	}
	return tableInfo, nil
}

// WriteTables takes a directory and Table map and parses csv or tsv data files, validating table data. The function
// then writes the files files to cassandra.
func WriteTables(session *gocql.Session, tableInfo map[string]*Table, dName string) error {
	files, err := ioutil.ReadDir(dName)
	if err != nil {
		return err
	}

	for _, file := range files {
		fmt.Printf("Reading %s.\n", file.Name())
		parseInfo := strings.Split(file.Name(), ".")

		colInfo, present := tableInfo[parseInfo[0]]
		dateColNames := ""
		if present != true {
			fmt.Println("\n\nThe table data file did not match any table in Cassandra.")
			continue
		} else {
			for colName, colType := range colInfo.Columns {
				// Check for columns that are dates, store their names
				if strings.Contains("datetimestamp", colType) {
					dateColNames += fmt.Sprintf("%s,", colName)
				}
			}
			dateColNames = strings.TrimRight(dateColNames, ",")
		}

		fTable, err := os.Open(dName + "/" + file.Name())
		if err != nil {
			log.Printf("No data file could be found: \n%v\n\nContinuing to next data file.\n", err)
		}

		var dateColLocs []int
		csvReader := csv.NewReader(fTable)
		if parseInfo[1] == "csv" {
			csvReader.Comma = ','
		} else {
			csvReader.Comma = '\t'
		}
		records, _ := csvReader.ReadAll()

		queryInsert := fmt.Sprintf("INSERT INTO %s", strings.ToLower(parseInfo[0]))
		var queryColOrder []string
		var rowColOrder []string
		queryValOrder := ""

		batch := gocql.NewBatch(gocql.LoggedBatch)
		for line, record := range records {
			if line == 0 {
				// Collect indices of date/time columns and use them for formatting
				for i, v := range record {
					rowColOrder = append(rowColOrder, strings.ToLower(v))
					if strings.Contains(dateColNames, strings.ToLower(v)) {
						dateColLocs = append(dateColLocs, i)
					}
				}
			} else {
				// iterate through the row and append to the query
				dateLocSet := make(map[int]bool) // create a set for O(1) lookup of matching date columns
				for _, v := range dateColLocs {
					dateLocSet[v] = true
				}

				for i, v := range record {
					var parsedDate time.Time
					layout := "02-Jan-06"
					if v == "" { // if the column has no value, don't bother adding to INSERT
						continue
					}
					// append to the current Column order, the column for the query
					queryColOrder = append(queryColOrder, rowColOrder[i])
					_, dateCol := dateLocSet[i]
					if dateCol {
						parsedDate, err = time.Parse(layout, v)
						if err != nil { // if the date can't be parsed, just don't insert
							log.Println("Error parsing date:", err)
							continue
						}
						queryValOrder += fmt.Sprintf("$$%s$$,", parsedDate.Format("2006-01-02"))
					} else { // if the column is not a date or timestamp, add to query
						if tableInfo[parseInfo[0]].Columns[rowColOrder[i]] == "text" {
							queryValOrder += fmt.Sprintf("$$%s$$,", v)
						} else if tableInfo[parseInfo[0]].Columns[rowColOrder[i]] == "uuid" {
							uuid, _ := gocql.RandomUUID()
							queryValOrder += fmt.Sprintf("$$%v$$,", uuid)
						} else if tableInfo[parseInfo[0]].Columns[rowColOrder[i]] == "boolean" {
							queryValOrder += fmt.Sprintf("%t,", v != "0")
						} else {
							_, err = strconv.ParseFloat(v, 64)
							if err != nil {
								queryValOrder += fmt.Sprintf("$$%s$$,", v)
							} else {
								queryValOrder += fmt.Sprintf("%s,", v)
							}
						}
					}
				}
				_, ok := tableInfo[parseInfo[0]].Columns[strings.ToLower(parseInfo[0][0:len(parseInfo[0])-1]+"_uuid")]
				if ok {
					queryColOrder = append(queryColOrder, strings.ToLower(parseInfo[0][0:len(parseInfo[0])-1]+"_uuid"))
					uuid, _ := gocql.RandomUUID()
					queryValOrder += fmt.Sprintf("%v,", uuid)
				}

				queryValOrder = strings.TrimRight(queryValOrder, ",")
				// fmt.Printf("\nqueryValOrder = %+v\n", queryValOrder)
				// fmt.Printf("\nqueryColOrder = %+v\n", queryColOrder)
				// create finished query
				query := fmt.Sprintf("%s (%s) VALUES (%s)", queryInsert, strings.Join(queryColOrder, ","), queryValOrder)
				// err = session.Query(query).Exec()
				// if err != nil {
				//     log.Println(err)
				//     fmt.Println(query)
				//     return err
				// }
				// fmt.Printf("\rWrote %d records.", line)

				// add to batch query
				batch.Query(query)
				query = ""
				queryValOrder = ""
				queryColOrder = nil
			}

			// every 25 lines, execute the batch
			if line%25 == 0 && line != 0 {
				fmt.Printf("\rLoading batch %d.", int(line/25))
				err = session.ExecuteBatch(batch)
				if err != nil {
					log.Println(err)
					fmt.Println(batch)
					return err
				}
				batch = gocql.NewBatch(gocql.LoggedBatch)
			}
			line++
		}

		err = session.ExecuteBatch(batch)
		fmt.Println("\rLoading last batch.")

		fTable.Close()
	}

	return nil
}

func main() {
	// Open conf and unmarshal into a Configuration struct.
	gConf, err := LoadConfig("../../config/conf.json")
	if err != nil {
		log.Fatalf("No config file could be found: \n%v", err)
	}

	cluster := gocql.NewCluster(gConf.Nodes...) // connect to the cluster
	cluster.Timeout = time.Duration(10) * time.Second
	cluster.Keyspace = gConf.Keyspace // use this as the default kespace
	cluster.Consistency = gocql.Quorum
	session, err := cluster.CreateSession()
	if err != nil {
		log.Fatalf("Something went wrong trying to connect to the DB: \n%v", err)
	}
	defer session.Close()

	tableInfo, err := LoadSchema("../../config/schema.json")
	if err != nil {
		log.Fatalf("The following error occured when parsing table schema: \n%v", err)
	}

	// Check if the SQL MAIN tables exist. Create them in they don't
	keySpaceMeta, _ := session.KeyspaceMetadata(gConf.Keyspace)
	for k, v := range tableInfo {
		// Cassandra uses lowercase for names
		tableName := strings.ToLower(k)
		if _, exists := keySpaceMeta.Tables[tableName]; exists != true { // generate create table query
			fmt.Printf("\n\n%s does not exist in %s.\n", k, gConf.Keyspace)
			tableCols := v.Columns
			query := fmt.Sprintf("CREATE TABLE %s (", k)
			for colName, colType := range tableCols {
				query += fmt.Sprintf(" %s %s,", colName, colType)
			}
			primaryKey := fmt.Sprintf(" PRIMARY KEY (%s))", v.Primary)
			query += primaryKey
			fmt.Printf("Executing %s\n", query)
			err = session.Query(query).Exec()
			if err != nil {
				log.Printf("There was a problem creating %s. Continuing.", k)
			}
		}
	}

	fmt.Println()
	fAbsPath, _ := filepath.Abs("../../assets/")
	WriteTables(session, tableInfo, fAbsPath)

	fmt.Println("\n\nDone.")
}
